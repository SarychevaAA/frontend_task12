/*
Реализовать функционал, что после заполнения формы и нажатия кнопки 'Подтвердить' - новый фильм добавляется в список. Страница не должна перезагружаться.
Новый фильм должен добавляться в movieDB.movies.
Для получения доступа к значению input - обращаемся к нему как input.value;

P.S. Здесь есть несколько вариантов решения задачи, принимается любой, но рабочий.
▸ Если название фильма больше, чем 21 символ - обрезать его и добавить три точки
▸ При клике на мусорную корзину - элемент будет удаляться из списка (сложно)
▸ Если в форме стоит галочка 'Сделать любимым' - в консоль вывести сообщение: 'Добавляем любимый фильм'
▸ Фильмы должны быть отсортированы по алфавиту
*/

'use strict';

const movieDB = {
    movies: [
        "Логан",
        "Лига справедливости",
        "Ла-ла лэнд",
        "Одержимость",
        "Скотт Пилигрим против..."
    ]
};

const deleteAds = () => {
    const adv = document.querySelectorAll('.promo__adv img');
    adv.forEach((elem)=>{
        elem.remove();
    })
}

const changeContext = (elem, text) =>{
    elem.textContent = text;
}

const changeBackground = (elem, imgLink) =>{
    elem.style.backgroundImage = imgLink;
}

const sortMoviesList = ()=>{
    movieDB.movies.sort();
}
const updateMoviesList = (films, node)=>{
    sortMoviesList(films);
    node.innerHTML = '';
    films.forEach((item, index)=>{
        console.log(item);
        node.innerHTML += `
            <li class = "promo__interactive-item">${index+1}. ${item}
              <div class = "delete"></div>
            </li>
        `;
    });
    document.querySelectorAll('.delete').forEach((button, index)=>{
        button.addEventListener('click', ()=>{
            button.parentElement.remove();
            movieDB.movies.splice(index, 1);
            updateMoviesList(films, node);
        })
    })
}

document.addEventListener('DOMContentLoaded', ()=>{
    const genre = document.querySelector('.promo__genre');
    const promoImg = document.querySelector('.promo__bg');
    const moviesList = document.querySelector('.promo__interactive-list');
    const urlNewBackground = 'url("img/bg.jpg")';
    const addForm = document.querySelector('form.add');
    const addInput = addForm.querySelector('.adding__input');
    const checkbox = addForm.querySelector('[type="checkbox"]');

    addForm.addEventListener('submit', (e)=>{
        e.preventDefault();
        const newFilm = addInput.value;
        const isFavorite = checkbox.checked;
        if (newFilm.length > 0){
            movieDB.movies.push(newFilm.length > 21 ? `${newFilm.slice(0, 21)}...`: newFilm);
        }
        if (isFavorite){
            console.log('Добавляем любимый фильм');
        }
        updateMoviesList(movieDB.movies, moviesList);
        e.target.reset();
    });
    deleteAds();
    changeContext(genre, "драма");
    changeBackground(promoImg, urlNewBackground);
    updateMoviesList(movieDB.movies, moviesList);
})
